package animals;

public interface Animal {
    // ...
    // many useful methods
    // ...

    void accept(AnimalVisitor visitor);
}
