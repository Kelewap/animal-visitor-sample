package animals;

public interface AnimalVisitor {
    void visit(Dog dog);
    void visit(Cat cat);
    void visit(Fish fish);
    void visit(Giraffe giraffe);
    void visit(Dingo dingo);
}
