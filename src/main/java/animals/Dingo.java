package animals;

// beware of extends-relationship - definition of accept() is not forced at compile-time
public class Dingo extends Dog {
    @Override
    public void accept(AnimalVisitor visitor) {
        // without this method overridden by Dingo, it would be visited as ordinary Dog
        visitor.visit(this);
    }
}
