package animals;

public class FeedingAnimalVisitor implements AnimalVisitor {
    @Override
    public void visit(Dog dog) {
        System.out.println("hahuahu");
    }

    @Override
    public void visit(Cat cat) {
        System.out.println("meow");
    }

    @Override
    public void visit(Fish fish) {
        System.out.println();
    }

    @Override
    public void visit(Giraffe giraffe) {
        System.out.println("ihaha");
    }

    @Override
    public void visit(Dingo dingo) {
        System.out.println("dingo hau");
    }
}
