package animals;

public class Main {
    public static void main(String[] args) {
        feedAnimal(getAbsolutelyRandomAnimal());
    }

    private static Animal getAbsolutelyRandomAnimal() {
        return new Dingo();
    }

    private static void feedAnimal(Animal animal) {
        FeedingAnimalVisitor feedingAnimalVisitor = new FeedingAnimalVisitor();

        animal.accept(feedingAnimalVisitor);
    }
}
